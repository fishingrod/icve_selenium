from selenium import webdriver
from time import perf_counter
import time,os
start = perf_counter()
wd = webdriver.Chrome()
wd.implicitly_wait(20)
wd.get('https://zjy2.icve.com.cn/portal/login.html')

#自己手动登陆

def get_class():
    global href
    unit_title = input('请输入你想刷取进度的课程名称(前几个字即可):')
    for t in [3,2,1]:
        print('{}秒后开始运行'.format(t))
        time.sleep(1)

    for class_name in wd.find_elements_by_css_selector('#noCourse .kt-lesson'):
        if unit_title in class_name.get_attribute('outerHTML'):
            print('\n<课程 {} 开始刷取>'.format(class_name.find_element_by_css_selector('.kt-lesson-title div').text))
            print('---运行过程中请切勿在此移动或点击其他网页，以免报错!---\n')
            href = class_name.find_element_by_css_selector('.kt-lesson-cover').get_attribute('href')
            wd.get(href)
            break

def Unit(): #课程大单元
    time.sleep(1)
    if is_element_exist('[class="am-icon sh-toc-expand module_condensation am-icon-caret-down"]'):
        for closed in wd.find_elements_by_css_selector('[class="am-icon sh-toc-expand module_condensation am-icon-caret-down"]'): # 先把全部默认打开的单元给收起
            try:
                closed.click()
            except Exception as e:
                if 'Other element would receive the click' in e:
                    pass

    global i
    for i in wd.find_elements_by_css_selector('.moduleList'):  # 定位大单元元素 —— 所有大单元的循环
        if '100%' in i.text:  #判断：如果此大单元进度已经完成，则打印'已完成，跳过此单元'
            print(i.find_element_by_css_selector('[title]').text, '单元已完成，自动跳过')
        else:  # 反之进行刷时
            print(i.find_element_by_css_selector('[title]').text, '单元未完成，展开刷时')
            time.sleep(0.2) #给程序反应时间
            i.click()  #展开大单元
            unit() # 运行小单元章节

def unit(): #小单元函数
    time.sleep(1)
    for I in i.find_elements_by_css_selector('.topicList'):  # 定位小单元元素 —— 在当前大单元下操作小单元
        time.sleep(0.2)  #给程序反应时间，点击展开小单元
        I.click()  # 展开小单元
        rop = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:100%;"'  # 判断小单元百分百的元素
        rop99 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:99%;"'  # 判断小单元百分百的元素
        rop98 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:98%;"'  # 判断小单元百分百的元素
        rop97 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:97%;"'  # 判断小单元百分百的元素
        rop5 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:5%;"'  # 判断小单元百分百的元素
        for unit in I.find_elements_by_css_selector('.sh-res'):  # 定位小单元中的文件元素
            if is_element_exist('[class="sh-res am-margin-left-xl openOrCloseCell"]') or is_element_exist('[class="sh-res-h am-margin-top-xs"]'): #在文件夹内的单元元素判断
                try:
                    if rop in unit.get_attribute('outerHTML'):  # 判断：如果此进度为100%，则打印'已完成'文本
                        print('    {}——已完成，自动跳过'.format(unit.find_element_by_css_selector('.isOpenModulePower').text))
                    elif rop99 in unit.get_attribute('outerHTML'):
                        print('    {}——99%进度bug，自动跳过'.format(
                            unit.find_element_by_css_selector('.isOpenModulePower').text))
                    elif rop98 in unit.get_attribute('outerHTML'):
                        print('    {}——98%进度bug，自动跳过'.format(
                            unit.find_element_by_css_selector('.isOpenModulePower').text))
                    elif rop97 in unit.get_attribute('outerHTML'):
                        print('    {}——97%进度bug，自动跳过'.format(
                            unit.find_element_by_css_selector('.isOpenModulePower').text))
                    elif rop5 in unit.get_attribute('outerHTML'):
                        print('    {}——5%进度bug，自动跳过'.format(

                            unit.find_element_by_css_selector('.isOpenModulePower').text))
                    else:  # 反之，执行刷取
                        print('    {}——未完成，正在刷取'.format(unit.find_element_by_css_selector('.isOpenModulePower').text),
                              end='')
                        for fill in unit.find_elements_by_css_selector('.isOpenModulePower'):  # for循环进小单元内进行点击刷取
                            fill.click()
                            decide()
                            Notext()
                except Exception as e:
                    print(e)
                    print(type(e))
            else:
                print('    --当前.topicList无元素，跳过')
                continue

        '''
        if is_element_exist('.isOpenModulePower'): #无文件夹
            for unit in I.find_elements_by_css_selector('.sh-res'):  # 定位小单元中的文件元素
                try:
                    if rop in unit.get_attribute('outerHTML'):  # 判断：如果此进度为100%，则打印'已完成'文本
                        print('    {}——已完成，自动跳过'.format(unit.find_element_by_css_selector('.isOpenModulePower').text))
                    elif rop99 in unit.get_attribute('outerHTML'):
                        print('    {}——99%进度bug，自动跳过'.format(
                            unit.find_element_by_css_selector('.isOpenModulePower').text))
                    elif rop98 in unit.get_attribute('outerHTML'):
                        print('    {}——98%进度bug，自动跳过'.format(
                            unit.find_element_by_css_selector('.isOpenModulePower').text))
                    else:  # 反之，执行刷取
                        print('    {}——未完成，正在刷取'.format(unit.find_element_by_css_selector('.isOpenModulePower').text),
                              end='')  # 定位单元名称
                        for fill in unit.find_elements_by_css_selector('.isOpenModulePower'):  # for循环进小单元内进行点击刷取
                            fill.click()  # 点击进入
                            decide()  # 进入后判断是视频还是压缩包文件，进行指定时间的time.sleep
                            Notext()  # 执行完成后，重新跑一遍展开程序，但是不print已完成的文本信息
                except:
                    DECIDE()

        rop = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:100%;"'  # 判断小单元百分百的元素
        rop99 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:99%;"'  # 判断小单元百分百的元素
        rop98 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:98%;"'  # 判断小单元百分百的元素
        if is_element_exist('[class="sh-res-h am-margin-left-xl "]') and is_element_exist('[class="sh-res am-margin-left-xl openOrCloseCell"]'): #判断是否是文件夹单元，并且其中有单元
            for unit in I.find_elements_by_css_selector('.sh-res'):  # 定位小单元中的文件元素
                try:
                    if rop in unit.get_attribute('outerHTML'):  # 判断：如果此进度为100%，则打印'已完成'文本
                        print('    {}——已完成，自动跳过'.format(unit.find_element_by_css_selector('.isOpenModulePower').text))
                    elif rop99 in unit.get_attribute('outerHTML'):
                        print('    {}——99%进度bug，自动跳过'.format(
                            unit.find_element_by_css_selector('.isOpenModulePower').text))
                    elif rop98 in unit.get_attribute('outerHTML'):
                        print('    {}——98%进度bug，自动跳过'.format(
                            unit.find_element_by_css_selector('.isOpenModulePower').text))
                    else:  # 反之，执行刷取
                        print('    {}——未完成，正在刷取'.format(unit.find_element_by_css_selector('.isOpenModulePower').text),
                              end='')  # 定位单元名称
                        for fill in unit.find_elements_by_css_selector('.isOpenModulePower'):  # for循环进小单元内进行点击刷取
                            fill.click()  # 点击进入
                            decide()  # 进入后判断是视频还是压缩包文件，进行指定时间的time.sleep
                            Notext()  # 执行完成后，重新跑一遍展开程序，但是不print已完成的文本信息
                except:
                    DECIDE()
        else:
            print('    --当前.topicList无元素，跳过')
        '''

def decide(): # 判断刷的是视频还是文件，决定等待时间
    time.sleep(10)
    if is_element_exist('#docPlayDiv video'): #判断，如果页面内元素有一个为video的元素，此元素为视频模块独有元素
        start = wd.find_element_by_css_selector('[class="jw-group jw-controlbar-left-group jw-reset"] > [class="jw-text jw-reset jw-text-elapsed"]')
        end = wd.find_element_by_css_selector('[class="jw-group jw-controlbar-right-group jw-reset"] > [class="jw-text jw-reset jw-text-duration"]')
        if '00:00' in end.get_attribute('outerHTML'): # 如果加载未完成
            wd.refresh() # 刷新页面
            time.sleep(10)

        if end.get_attribute('outerHTML').count(':') == 1: #判断视频时长，如果只有一个:则为一小时以内的
            try:
                duration = (int(end.text[:2]) * 60 + int(end.text[-2:])) - (
                        int(start.text[:2]) * 60 + int(start.text[-2:]))
            except:
                start = start.get_attribute('outerHTML')[47:52]
                end = end.get_attribute('outerHTML')[48:53]
                duration = (int(end[:2]) * 60 + int(end[-2:])) - (int(start[:2]) * 60 + int(start[-2:]))
        elif end.get_attribute('outerHTML').count(':') == 2: #如果有两个:时长超过一个小时
            if start.get_attribute('outerHTML').count(':') == 1: #判断当前进度时长，如果在一个小时内
                try:
                    duration = (int(end.text[0]) * 3600 + int(end.text[2:4]) * 60 + int(end.text[-2:])) - (
                            int(start.text[:2]) * 60 + int(start.text[-2:]))
                except:
                    start = start.get_attribute('outerHTML')[47:52]
                    end = end.get_attribute('outerHTML')[48:55]
                    duration = (int(end[0]) * 3600 + int(end[2:4]) * 60 + int(end[-2:])) - (
                            int(start[:2]) * 60 + int(start[-2:]))
            elif start.get_attribute('outerHTML').count(':') == 2: #判断当前进度时长，如果大于一个小时
                try:
                    duration = (int(end.text[0]) * 3600 + int(end.text[2:4]) * 60 + int(end.text[-2:])) - (
                            int(start.text[0]) * 3600 + int(start.text[2:4]) * 60 + int(start.text[-2:]))
                except:
                    start = start.get_attribute('outerHTML')[47:54]
                    end = end.get_attribute('outerHTML')[48:55]
                    duration = (int(end[0]) * 3600 + int(end[2:4]) * 60 + int(end[-2:])) - (
                                int(start[0]) * 3600 + int(start[2:4]) * 60 + int(start[-2:]))

        if duration <= 60:
            print('    -60秒后返回-', end='')
            time.sleep(60)
        else:
            print('    -{}秒后返回-'.format(duration), end='')
            time.sleep(duration + 5)
            # while duration>=0:
            #     time.sleep(60) #每60秒循环检测一次是否弹出《学习过程中异常》
            #     if is_element_exist('.popBox [class="sgBtn ok"]'):
            #         print('学习过程异常错误',end='')
            #         wd.refresh()
            #         time.sleep(1)
            #         wait = eval(wd.find_element_by_css_selector('.txtBox').text[17:19])
            #         time.sleep(wait+1*60)
        print('    -视频已完成-')
        wd.back()
    else: #反之继续判断
        if is_element_exist('#docPlayDiv .MPreview-box'): #是否为文档类模块，此元素也为翻页文档独有
            for a in range(int(wd.find_element_by_css_selector('.MPreview-pageCount em').text)):
                time.sleep(0.2)
                wd.find_element_by_css_selector('[class="page-bar-inner clefix"] [class="MPreview-pageNext current"] span').click()
            # print('    -文档已完成-')
            time.sleep(30)
        elif is_element_exist('.am-u-sm-12 [class="np-link-go-hd am-text-warning"]'): #反之为普通文档
            time.sleep(3)
            # print('    -压缩包已完成-')
        elif is_element_exist('#docPlayDiv iframe'):
            time.sleep(5)
            for c in range(100):
                time.sleep(0.2)
                wd.find_element_by_css_selector('.stage-next .stage-next-btn').click()
            time.sleep(10)
            # print('    -PPT已完成-')
        else:
            DECIDE()
        print('    -文件已完成-')
        wd.back()
    # print('    -已完成-')

def DECIDE():
    time.sleep(1)
    wd.get(href)
    time.sleep(1)
    if is_element_exist('[class="am-tabs np-tabs"]'):  # 判断是否进入开始刷取
        if is_element_exist('.linkBody > .link'):
            # if '上次学习课件' in wd.find_element_by_css_selector('.linkBody > .link').text:  # 如果是学习课件网页
            print('    <误入课件学习提示网页>', end='')
            wd.find_element_by_css_selector('[class="link lastLink"] #studyNow').click()
            decide()
            Notext()
        print('    <小单元退出失败，正在重新退出执行程序>')
        wd.back()
        Notext()
    elif is_element_exist('#uSidebarStu [class="lSidebar sidebarSelected"]'): # 主页旁的单元元素
        if '首页' in wd.find_element_by_css_selector('#uSidebarStu [class="lSidebar sidebarSelected"]').get_attribute('outerHTML'): # 判断如果在首页
            print('    <当前为首页界面，正在返回原课程>')
            wd.get(href)
            Notext()
        elif '我的课程' in wd.find_element_by_css_selector('#uSidebarStu [class="lSidebar sidebarSelected"]').get_attribute('outerHTML'):
            Notext()
    elif is_element_exist('.popBox [class="sgBtn ok"]'):
        print('异常错误(网络波动或其他异常)自动退出')
        wd.find_element_by_css_selector('.popBox [class="sgBtn ok"]').click()
        print('程序运行时间:{}'.format(perf_counter() - start))
        quit()
    else:
        Notext()

def Notext(): #删除了 print已完成自动跳过 的文本信息
    time.sleep(1)
    if is_element_exist('[class="am-icon sh-toc-expand module_condensation am-icon-caret-down"]'):
        for closed in wd.find_elements_by_css_selector('[class="am-icon sh-toc-expand module_condensation am-icon-caret-down"]'): # 先把全部默认打开的单元给收起
            try:
                closed.click()
            except:
                pass

    try:

        for i in wd.find_elements_by_css_selector('.moduleList'):  # 定位大单元元素 —— 所有大单元的循环
            if '100%' in i.text:  # 判断：如果此大单元进度已经完成，则跳过此单元
                pass
            else:  # 反之进行刷时
                time.sleep(0.2)  # 给程序反应时间，点击展开大单元
                i.click()  # 展开大单元
                time.sleep(1)
                for I in i.find_elements_by_css_selector('.topicList'):  # 定位小单元元素 —— 在当前大单元下操作小单元
                    time.sleep(0.2)  # 给程序反应时间，点击展开小单元
                    I.click()  # 展开小单元
                    rop = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:100%;"'  # 判断小单元百分百的元素
                    rop99 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:99%;"'  # 判断小单元百分百的元素
                    rop98 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:98%;"'  # 判断小单元百分百的元素
                    rop97 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:97%;"'  # 判断小单元百分百的元素
                    rop5 = 'style="position:absolute;height:22px;border-radius: 2px;background: #03ae84;top:-15px;left:0px;width:5%;"'  # 判断小单元百分百的元素
                    for unit in I.find_elements_by_css_selector('.sh-res'):  # 定位小单元中的文件元素
                        if is_element_exist('[class="sh-res am-margin-left-xl openOrCloseCell"]') or is_element_exist(
                                '[class="sh-res-h am-margin-top-xs"]'):  # 在文件夹内的单元元素判断
                            try:
                                if rop in unit.get_attribute('outerHTML'):  # 判断：如果此进度为100%，则打印'已完成'文本
                                    pass
                                elif rop99 in unit.get_attribute('outerHTML'):
                                    pass
                                elif rop98 in unit.get_attribute('outerHTML'):
                                    pass
                                elif rop97 in unit.get_attribute('outerHTML'):
                                    pass
                                elif rop5 in unit.get_attribute('outerHTML'):
                                    pass
                                else:  # 反之，执行刷取
                                    print('    {}——未完成，正在刷取'.format(
                                        unit.find_element_by_css_selector('.isOpenModulePower').text),
                                          end='')  # 定位单元名称
                                    for fill in unit.find_elements_by_css_selector(
                                            '.isOpenModulePower'):  # for循环进小单元内进行点击刷取
                                        fill.click()  # 点击进入
                                        decide()  # 进入后判断是视频还是压缩包文件，进行指定时间的time.sleep
                                        Notext()  # 执行完成后，重新跑一遍展开程序，但是不print已完成的文本信息
                            except Exception as e:
                                print(e)
                                print(type(e))
                        else:
                            print('    --当前.topicList无元素，跳过')
                            continue
    except:
        DECIDE()

def is_element_exist(location_address):
    element = wd.find_elements_by_css_selector(location_address)
    if len(element) == 0:
        return False
    elif len(element) == 1:
        return True
    else:
        return True

def main():
    print('请手动登陆')

    waiting_num = 0
    while True:
        if is_element_exist('.ad-cancel'):
            wd.find_element_by_css_selector('.ad-cancel').click()
            break
        elif waiting_num == 3:
            print('登陆超时，结束运行')
            print('程序运行时间:{}'.format(perf_counter() - start))
            quit()
        else:
            print('-登陆时间过长，请尽快登录-')
            waiting_num += 1
            time.sleep(5)

    get_class()
    time.sleep(1)
    Unit()

try:
    if __name__ == '__main__':
        main()
    print('\n运行完成')
    print('程序运行时间:{}'.format(perf_counter() - start))
    input('<输入回车键结束程序>')
except Exception as e:
    print('运行错误')
    print(e)
    print('程序运行时间:{}'.format(perf_counter() - start))
    input('<输入回车键结束程序>')

#乐