# 职教云课程进度脚本

#### 介绍
个人早期自学selenium作品，用于刷取职教云多课程进度
早版本的屎山，不过现在还能使用=.=

#### 软件架构
软件架构说明


#### 安装教程

1.  脚本同目录下还需一份相应版本的浏览器驱动，本人使用的是谷歌浏览器，下载见：https://chromedriver.storage.googleapis.com/index.html
2.  使用到selenium、time、os库，运行.py脚本需要相应库，而.exe打包文件运行不用
3.  大部分情况能正常运行

#### 使用说明

1.  运行后手动登陆，键盘输入想要刷取的课程名字，名字不完整也没问题。但是不要简写，如：毛zx思想与中国特色社会主义...————毛概
2.  键入后自动运行该课程进度的刷取，内有大部分异常处理
3.  个人早期堆叠的屎山，不过至2022年还能使用

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
